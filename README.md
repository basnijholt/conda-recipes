# Conda build recipes [Anaconda.org/conda-forge](https://anaconda.org/conda-forge/)

Install stable `kwant` with:
```
conda install -c conda-forge kwant
```

Install latest master `kwant` with:
```
conda install -c kwant kwant=dev
```

In order to install [Kwant](https://kwant-project.org/) you need some non-Python packages, therefore I created `conda-build` recipes to compile everything.

  - [MUMPS](http://mumps.enseeiht.fr) Sequential version, with SCOTCH and Metis (non-python)
  - [SCOTCH](https://www.labri.fr/perso/pelegrin/scotch/) (non-python)
  - [Metis](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) (non-python)
  - [Tinyarray](http://kwant-project.org/)
  - [Kwant](https://gitlab.kwant-project.org/kwant/tinyarray)
